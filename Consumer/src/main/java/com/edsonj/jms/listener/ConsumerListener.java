package com.edsonj.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.edsonj.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {
	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private ConsumerAdapter consumerAdapter;

	@Override
	public void onMessage(Message message) {
		logger.info("in onMessage()");
		String json = null;
		if(message instanceof TextMessage) {
			try {
				json = ((TextMessage)message).getText();
				logger.info("Sending JSON to DB: " + json);
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				jmsTemplate.convertAndSend(json);
				logger.error("Message:" + json);
			}
		}
	}

}
