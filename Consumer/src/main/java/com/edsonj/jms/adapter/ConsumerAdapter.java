package com.edsonj.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());

	public void sendToMongo(String json) {
		logger.info("Sending to MongoDB");
		MongoClient client = new MongoClient();
		DB database = client.getDB("vendor");
		DBObject obj = (DBObject)JSON.parse(json);
		if( !database.collectionExists("contact"))
			database.createCollection("contact", obj);
		DBCollection collection = database.getCollection("contact");
		logger.info("Converting JSON to DBObject");
		collection.insert(obj);
	}

}
